﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;

namespace HangmanMediaKmutt
{
    class Program
    {
        //===============================================================================================
        // helper function
        //===============================================================================================
        static int calculatePowerOfTwoIndex(int stringIndex, int stringLength)
        {
            return (stringLength - 1) - stringIndex;

        }

        static int calculatePowerOfTwoPosition(int power)
        {
            return (int)Math.Pow(2, power);
        }

        static void PlayCorrectSound()
        {
            PlaySound("Speech On");
        }

        static void PlayInCorrectSound()
        {
            PlaySound("Speech Off");
        }

        static void PlayEndGameWinSound()
        {
            PlaySound("Ring08");
        }

        static void PlayEndGameLoseSound()
        {
            PlaySound("Ring09");
        }

        static void PlaySound(string soundFileName)
        {
            try
            {
                var simpleSound = new SoundPlayer(@"C:\Windows\Media\" + soundFileName + ".wav");
                simpleSound.PlaySync();
            }
            catch { }
        }

        //end of helper function ========================================================================

        //===============================================================================================
        // custom function
        //===============================================================================================

        //end of custom function ========================================================================


        //===============================================================================================
        // template function
        //===============================================================================================
        static void printHangman(int life)
        {
            if (life == 4)
            {
                Console.WriteLine("____");
                Console.WriteLine("|   |");
                Console.WriteLine("|    ");
                Console.WriteLine("|    ");
                Console.WriteLine("|    ");
                Console.WriteLine("|_______ ");
            }
            else if (life == 3)
            {
                Console.WriteLine("____");
                Console.WriteLine("|   |");
                Console.WriteLine("|   o ");
                Console.WriteLine("|     ");
                Console.WriteLine("|     ");
                Console.WriteLine("|_______ ");
            }
            else if (life == 2)
            {
                Console.WriteLine("____");
                Console.WriteLine("|   |");
                Console.WriteLine("|   o ");
                Console.WriteLine("|   | ");
                Console.WriteLine("|     ");
                Console.WriteLine("|_______ ");
            }
            else if (life == 1)
            {
                Console.WriteLine("____");
                Console.WriteLine("|   |");
                Console.WriteLine("|   o ");
                Console.WriteLine("|  /|\\ ");
                Console.WriteLine("|       ");
                Console.WriteLine("|_______ ");
            }
            else if (life == 0)
            {
                Console.WriteLine("____");
                Console.WriteLine("|   |");
                Console.WriteLine("|   o ");
                Console.WriteLine("|  /|\\ ");
                Console.WriteLine("|  / \\ ");
                Console.WriteLine("|_______ ");
            }
        }

        static int convertToCheckerPositionFlag(int valueLength)
        {
            int checkerPositionFlag = 0;
            for (int idx = 0; idx < valueLength; idx++)
            {
                checkerPositionFlag = checkerPositionFlag + calculatePowerOfTwoPosition(idx);
            }
            return checkerPositionFlag;
        }

        static void printCurrentAnswerCorrect(int currentCorrectPositionFlag, string answer)
        {
       
            for (int index = 0; index < answer.Length; index += 1)
            {
                int powerindex= calculatePowerOfTwoIndex(index, answer.Length);
                int powerValue = calculatePowerOfTwoPosition(powerindex);
                if ((powerValue & currentCorrectPositionFlag) == powerValue)
                {
                    Console.Write(answer[index]);
                }
                else Console.Write(" _ ");
            }
            
        }

        static char getInputGuessChar()
        {
            System.Console.Write("get input guess char:  ");
            char guessChar = char.Parse(Console.ReadLine());
            return guessChar;
        }

        static int getNewCorrectPositionFlag(char guessChar, int currentCorrectPositionFlag, string answer)
        {

            int newCorrectPosition = currentCorrectPositionFlag;
            for (int i = 0; i <= answer.Length - 1; i++)
            {
                if (guessChar == answer[i])
                {
                    int power = calculatePowerOfTwoIndex(i, answer.Length);
                    int ans = calculatePowerOfTwoPosition(power);
                    newCorrectPosition = newCorrectPosition | ans;
                }
            }
            return newCorrectPosition;
        }

        static bool checkIsInputCorrect(int currentCorrectPositionFlag, int newCorrectPositionFlag)
        {
            return true;
        }

        static bool isEndGame(int life, int currentCorrectPositionFlag, int checkerPositionFlag)
        {
            return true;
        }

        //end of template function ======================================================================

        static void Main(string[] args)
        {
            int life = 4;
            string answer = "kmutt";
            string hint = "Best college in Thailand";
            int answerLength = answer.Length;
            int checkerPositionFlag = convertToCheckerPositionFlag(answerLength);
            int currentCorrectPositionFlag = 0;

            Console.WriteLine("Welcome to Hangman game");
            Console.WriteLine("Rules you need to guess word from hint message, you have {0} chance to guess word otherwise game will be over", life);
            Console.WriteLine("==============================================");
            Console.WriteLine("Hint: {0}", hint);
            Console.WriteLine("==============================================");

            do
            {
                printHangman(life);

                Console.WriteLine();
                printCurrentAnswerCorrect(currentCorrectPositionFlag, answer);
                Console.WriteLine("\n");

                Console.WriteLine("Chance left: {0}", life);
                char guessChar = getInputGuessChar();
                Console.WriteLine("\nYou guess: {0}", guessChar);

                int newCorrectPositionFlag = getNewCorrectPositionFlag(guessChar, currentCorrectPositionFlag, answer);
                bool isInputCorrect = checkIsInputCorrect(currentCorrectPositionFlag, newCorrectPositionFlag);
                currentCorrectPositionFlag = newCorrectPositionFlag;

                if (isInputCorrect == false)
                {
                    life -= 1;
                    Console.WriteLine("Boooooo ! this {0} character not exist in answer", guessChar);
                }

                Console.WriteLine("=====================================================");

            } while (isEndGame(life, currentCorrectPositionFlag, checkerPositionFlag));

            if (life <= 0)
            {
                printHangman(life);
                Console.WriteLine("Game over !");
            }
            else
            {
                Console.WriteLine("Answer = {0}", answer);
                Console.WriteLine("You win !");
            }

            Console.ReadLine();
        }
    }
}
